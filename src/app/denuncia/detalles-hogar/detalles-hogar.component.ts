import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { ConductorService } from '../../servicios/conductor.service';


interface Lugar {
  name: string;
  code: string;
}

@Component({
  selector: 'app-detalles-hogar',
  templateUrl: './detalles-hogar.component.html',
  styleUrls: ['./detalles-hogar.component.css']
})
export class DetallesHogarComponent implements OnInit {
  

  lug: Lugar[];

  selectedLug: Lugar;

  items: SelectItem[];

  item: string;

  checked2= true;
  
  displayModalTitular: boolean;

  displayModalFinalizar: boolean;

  conductor:any[] = [];

  showModalDialogTitular() {
    this.displayModalTitular = true;
  }

  showModalDialogFinalizar() {
    this.displayModalFinalizar = true;
  }

  constructor(private _conductorService:ConductorService) {

    this.lug = [
      {name: 'Vía Pública', code: 'VP'},
      {name: 'Local del Tercero', code: 'LT'},
      {name: 'Domicilio del asegurado', code: 'DA'},
      {name: 'Financieras o Bancos', code: 'SL'},
      {name: 'Transporte Público', code: 'TP'},
      {name: 'Vehículo Propio', code: 'VPR'},
      {name: 'Vehículo del tercero', code: 'VT'}
  ];

    this.items = [];
    for (let i = 0; i < 10000; i++) {
        this.items.push({label: 'Item ' + i, value: 'Item ' + i});
    }
}

  ngOnInit() {

    this.conductor = this._conductorService.getConductor();

  }

}
