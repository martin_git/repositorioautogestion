import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesHogarComponent } from './detalles-hogar.component';

describe('DetallesHogarComponent', () => {
  let component: DetallesHogarComponent;
  let fixture: ComponentFixture<DetallesHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
