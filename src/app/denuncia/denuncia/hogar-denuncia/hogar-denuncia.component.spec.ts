import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HogarDenunciaComponent } from './hogar-denuncia.component';

describe('HogarDenunciaComponent', () => {
  let component: HogarDenunciaComponent;
  let fixture: ComponentFixture<HogarDenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HogarDenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HogarDenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
