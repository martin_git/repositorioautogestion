import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hogar-denuncia',
  templateUrl: './hogar-denuncia.component.html',
  styleUrls: ['./hogar-denuncia.component.css']
})
export class HogarDenunciaComponent implements OnInit {

  denuncia_1 = 'Daño a Terceros';
  denuncia_2 = 'Impacto de Vehículo';
  denuncia_3 = 'Hoyo en Uno';
  denuncia_4 = 'Tumulto Polpular';
  denuncia_5 = 'Otros';


  constructor() { }

  ngOnInit() {
  }

}
