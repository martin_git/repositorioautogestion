import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoDenunciaComponent } from './auto-denuncia.component';

describe('AutoDenunciaComponent', () => {
  let component: AutoDenunciaComponent;
  let fixture: ComponentFixture<AutoDenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoDenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoDenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
