import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auto-denuncia',
  templateUrl: './auto-denuncia.component.html',
  styleUrls: ['./auto-denuncia.component.css']
})
export class AutoDenunciaComponent implements OnInit {

  denuncia_1 = 'Accidente';
  denuncia_2 = 'Incendio';
  denuncia_3 = 'Robo';
  denuncia_4 = 'Efectos de la naturaleza';
  denuncia_5 = 'Otros';


  constructor() { }

  ngOnInit() {
  }

}
