import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-denunciar',
  templateUrl: './denunciar.component.html',
  styleUrls: ['./denunciar.component.css']
})
export class DenunciarComponent implements OnInit {

  denuncia_1 = 'Accidente';
  denuncia_2 = 'Incendio';
  denuncia_3 = 'Robo';
  denuncia_4 = 'Efectos de la naturaleza';
  denuncia_5 = 'Otros';


  constructor() { }

  ngOnInit() {
  }

}
