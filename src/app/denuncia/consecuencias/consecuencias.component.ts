import { Component, OnInit } from '@angular/core';
import { IdentificarService } from '../../servicios/identificar.service';
import { TerceroService } from '../../servicios/tercero.service';
import { VehiculoService } from '../../servicios/vehiculo.service';
import { CosaService } from '../../servicios/cosa.service';
import { PolizaDiezService } from '../../servicios/auto/polizaDiez.service';
import { SelectItem } from 'primeng/api';

interface Afectado {
  name: string;
  code: string;
}

@Component({
  selector: 'app-consecuencias',
  templateUrl: './consecuencias.component.html',
  styleUrls: ['./consecuencias.component.css']
})

export class ConsecuenciasComponent implements OnInit {


  description: string;

  terceros= false;

  vehiculos= false;

  cosas= false;

  detalle= false;

  afecta1: SelectItem[];

  selectedAfec1: Afectado;

  checkedTercero: boolean = false;

  checkedVehiculo: boolean = false;

  checkedCosa: boolean = false;

  checked4: boolean = false;

  checkedDano: boolean = false;

  checked: boolean = false;

  identificar:any[] = [];

  tercero:any[] = [];

  vehiculo:any[] = [];

  cosa:any[] = [];

  polizaDiez:any[] = [];

  displayModalTerceros: boolean;

  displayModalVehiculos: boolean;

  displayModalCosas: boolean;

  displayModalCosa: boolean;

  displayModalGuardar: boolean;

  position: string;

  showModalDialogTerceros() {
    this.displayModalTerceros = true;
  }

  showModalDialogVehiculos() {
    this.displayModalVehiculos = true;
  }

  showModalDialogCosas() {
    this.displayModalCosa = true;
  }

  showModalDialogGuardar() {
    this.displayModalGuardar = true;
  }



  constructor( private _identificarService:IdentificarService,
               private _terceroService:TerceroService,
               private _vehiculoService:VehiculoService,
               private _cosaService:CosaService,
               private _polizaDiezService:PolizaDiezService) {

                


    
}

  ngOnInit() {

    this.identificar = this._identificarService.getIdentificar();

    this.tercero = this._terceroService.getTercero();

    this.vehiculo = this._vehiculoService.getVehiculo();

    this.cosa = this._cosaService.getCosa();

    this.polizaDiez = this._polizaDiezService.getPolizaDiez()
    
  
    }


}