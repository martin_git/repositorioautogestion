import { Component, OnInit } from '@angular/core';
import { IdentificarService } from '../../servicios/identificar.service';
import { TerceroService } from '../../servicios/tercero.service';
import { VehiculoService } from '../../servicios/vehiculo.service';
import { CosaService } from '../../servicios/cosa.service';
import { PolizaTodoRiesgoHogarService } from '../../servicios/hogar/polizaTodoRiesgoHogar.service';
import { SelectItem } from 'primeng/api';

interface Afectado {
  name: string;
  code: string;
}

@Component({
  selector: 'app-consecuencias-hogar',
  templateUrl: './consecuencias-hogar.component.html',
  styleUrls: ['./consecuencias-hogar.component.css']
})

export class ConsecuenciasHogarComponent implements OnInit {


  description: string;

  terceros= false;

  vehiculos= false;

  cosas= false;

  detalle= false;

  afecta1: SelectItem[];

  selectedAfec1: Afectado;

  checkedTercero: boolean = false;

  checkedVehiculo: boolean = false;

  checkedCosa: boolean = false;

  checked4: boolean = false;

  checkedDano: boolean = false;

  checked: boolean = false;

  identificar:any[] = [];

  tercero:any[] = [];

  vehiculo:any[] = [];

  cosa:any[] = [];

  polizaTodoRiesgoHogar:any[] = [];

  displayModalTerceros: boolean;

  displayModalVehiculos: boolean;

  displayModalCosas: boolean;

  displayModalCosa: boolean;

  displayModalGuardar: boolean;

  position: string;

  showModalDialogTerceros() {
    this.displayModalTerceros = true;
  }

  showModalDialogVehiculos() {
    this.displayModalVehiculos = true;
  }

  showModalDialogCosas() {
    this.displayModalCosa = true;
  }

  showModalDialogGuardar() {
    this.displayModalGuardar = true;
  }



  constructor( private _identificarService:IdentificarService,
               private _terceroService:TerceroService,
               private _vehiculoService:VehiculoService,
               private _cosaService:CosaService,
               private _polizaTodoRiesgoHogar:PolizaTodoRiesgoHogarService) {

                


    
}

  ngOnInit() {

    this.identificar = this._identificarService.getIdentificar();

    this.tercero = this._terceroService.getTercero();

    this.vehiculo = this._vehiculoService.getVehiculo();

    this.cosa = this._cosaService.getCosa();

    this.polizaTodoRiesgoHogar  = this._polizaTodoRiesgoHogar.getPolizaTodoRiesgoHogar();
    
  
    }


}