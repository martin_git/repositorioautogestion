import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsecuenciasHogarComponent } from './consecuencias-hogar.component';

describe('ConsecuenciasHogarComponent', () => {
  let component: ConsecuenciasHogarComponent;
  let fixture: ComponentFixture<ConsecuenciasHogarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsecuenciasHogarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsecuenciasHogarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
