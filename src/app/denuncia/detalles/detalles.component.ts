import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { ConductorService } from '../../servicios/conductor.service';

interface City {
  name: string;
  code: string;
}

interface Prov {
  name: string;
  code: string;
}

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit {
  
  cities: City[];

  prov: Prov[];

  selectedCity: City;

  selectedProv: Prov;

  items: SelectItem[];

  item: string;

  checked2= true;
  
  displayModalTitular: boolean;

  displayModalFinalizar: boolean;

  displayModalGuardar: boolean;

  conductor:any[] = [];

  showModalDialogTitular() {
    this.displayModalTitular = true;
  }

  showModalDialogFinalizar() {
    this.displayModalFinalizar = true;
  }

  showModalDialogGuardar() {
    this.displayModalGuardar = true;
  }

  constructor(private _conductorService:ConductorService) {
    this.cities = [
        {name: 'Villa Bllester', code: 'VB'},
        {name: 'San Martin', code: 'SM'},
        {name: 'Munro', code: 'M'},
        {name: 'Villa Adelina', code: 'VA'},
        {name: 'San Andres', code: 'SA'}
    ];

    this.prov = [
      {name: 'Buenos Aires', code: 'BA'},
      {name: 'Santa Fe', code: 'SF'},
      {name: 'Chaco', code: 'C'},
      {name: 'San Luis', code: 'SL'},
      {name: 'Entre Rios', code: 'ER'}
  ];

    this.items = [];
    for (let i = 0; i < 10000; i++) {
        this.items.push({label: 'Item ' + i, value: 'Item ' + i});
    }
}

  ngOnInit() {

    this.conductor = this._conductorService.getConductor();

  }

}
