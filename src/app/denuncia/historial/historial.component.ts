import { Component, OnInit } from '@angular/core';
import { HistorialService } from '../../servicios/historial.service';

export interface Historial {
  fecha;
  siniestro;
  poliza;
  riesgo;
  causa;
  state;
}

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  historial:any[] = [];

  constructor(private _historialService:HistorialService) { }

  ngOnInit() {

    this.historial = this._historialService.getHistorial();

    console.log(this.historial);


  }

}
