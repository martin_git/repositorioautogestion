import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsecuenciasDeRoboComponent } from './consecuencias-de-robo.component';

describe('ConsecuenciasDeRoboComponent', () => {
  let component: ConsecuenciasDeRoboComponent;
  let fixture: ComponentFixture<ConsecuenciasDeRoboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsecuenciasDeRoboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsecuenciasDeRoboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
