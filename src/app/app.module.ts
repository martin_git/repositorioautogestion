import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PasswordModule } from 'primeng/password';
import { DialogModule } from 'primeng/dialog';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ToastModule } from 'primeng/toast';
import { TabViewModule } from 'primeng/tabview';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';
import { CarouselModule } from 'primeng/carousel';
import { DropdownModule } from 'primeng/dropdown';
import { MessageService } from 'primeng/api';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { TableModule } from 'primeng/table';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { PaginatorModule } from 'primeng/paginator';
import { TooltipModule } from 'primeng/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterLinkActive } from '@angular/router';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { KeyFilterModule } from 'primeng/keyfilter';
import { GMapModule } from 'primeng/gmap';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { CalendarModule } from 'primeng/calendar';


// Servicios
import { PolizaService } from './servicios/poliza.service';
import { CoverturaService } from './servicios/covertura.service';
import { CoverturaMoreService } from './servicios/coverturaMore.service';
import { NeedService } from './servicios/need.service';
import { HistorialService } from './servicios/historial.service';
import { IdentificarService } from './servicios/identificar.service';
import { TerceroService } from './servicios/tercero.service';
import { VehiculoService } from './servicios/vehiculo.service';
import { CosaService } from './servicios/cosa.service';
import { ConductorService } from './servicios/conductor.service';
import { PolizaActivaService } from './servicios/auto/polizaActiva.service';
import { PolizaBasicaService } from './servicios/auto/polizaBasica.service';
import { PolizaBasicaINService } from './servicios/auto/polizaBasicaIN.service';
import { PolizaClasicaService } from './servicios/auto/polizaClasica.service';
import { PolizaDiezService } from './servicios/auto/polizaDiez.service';
import { PolizaPremiumCarService } from './servicios/auto/polizaPremiumCar.service';
import { PolizaTodoAutoService } from './servicios/auto/polizaTodoAuto.service';
import { PolizaUnoService } from './servicios/auto/polizaUno.service';
import { PolizaXpressCarService } from './servicios/auto/polizaXpressCar.service';
import { PolizaBasicaMasMotoService } from './servicios/moto/polizaBasicaMasMoto.service';
import { PolizaBasicaMotoService } from './servicios/moto/polizaBasicaMoto.service';
import { PolizaPremiumMotoService } from './servicios/moto/polizaPremiumMoto.service';
import { PolizaUnoMotoService } from './servicios/moto/polizaUnoMoto.service';
import { PolizaAbiertaHogarService } from './servicios/hogar/polizaAbiertaHogar.service';
import { PolizaModuladaHogarService } from './servicios/hogar/polizaModuladaHogar.service';
import { PolizaTodoRiesgoHogarService } from './servicios/hogar/polizaTodoRiesgoHogar.service';
import { PolizaMascotaHogarService } from './servicios/hogar/polizaMascotaHogar.service';
import { PolizaCintaAzulService } from './servicios/embarcaciones/polizaCintaAzul.service';
import { PolizaCintaMarineraService } from './servicios/embarcaciones/polizaCintaMarinera.service';
import { PolizaAPVidaService } from './servicios/vida/polizaAPVida.service';
import { PolizaAPPlatinoVidaService } from './servicios/vida/polizaAPPlatinoVida.service';
import { PolizaAP24VidaService } from './servicios/vida/polizaAP24Vida.service';
import { PolizaAPDeportivoVidaService } from './servicios/vida/polizaAPDeportivoVida.service';
import { PolizaPlusRVidaService } from './servicios/vida/polizaPlusRVida.service';
import { PolizaSepelioVidaService } from './servicios/vida/polizaSepelioVida.service';
import { PolizaSimpleVidaService } from './servicios/vida/polizaSimpleVida.service';

// Rutas
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { LogInitialComponent } from './login/log-initial/log-initial.component';
import { LogHomeComponent } from './login/log-home/log-home.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { HeaderComponent } from './dashboard/header/header.component';
import { MenuComponent } from './dashboard/menu/menu.component';
import { TitleComponent } from './dashboard/title/title.component';
import { SummaryComponent } from './dashboard/summary/summary.component';
import { ContentComponent } from './dashboard/content/content.component';
import { ActionComponent } from './dashboard/action/action.component';
import { NeedComponent } from './dashboard/need/need.component';
import { MoreComponent } from './dashboard/more/more.component';
import { MenuSlideComponent } from './dashboard/menu-slide/menu-slide.component';
import { FloatComponent } from './dashboard/float/float.component';
import { DenunciarComponent } from './denuncia/denunciar/denunciar.component';
import { HistorialComponent } from './denuncia/historial/historial.component';
import { ConsecuenciasComponent } from './denuncia/consecuencias/consecuencias.component';
import { DetallesComponent } from './denuncia/detalles/detalles.component';
import { TercerosComponent } from './modals/terceros/terceros.component';
import { VehiculosComponent } from './modals/vehiculos/vehiculos.component';
import { CosasComponent } from './modals/cosas/cosas.component';
import { TitularComponent } from './modals/titular/titular.component';
import { GuardarComponent } from './modals/guardar/guardar.component';
import { FinalizarComponent } from './modals/finalizar/finalizar.component';
import { ConsecuenciasDeRoboComponent } from './denuncia/consecuencias-de-robo/consecuencias-de-robo.component';
import { ConsecuenciasHogarComponent } from './denuncia/consecuencias-hogar/consecuencias-hogar.component';
import { DetallesHogarComponent } from './denuncia/detalles-hogar/detalles-hogar.component';
import { AutoComponent } from './dashboard/coberturas/auto/auto.component';
import { HogarComponent } from './dashboard/coberturas/hogar/hogar.component';
import { EmbarcacionComponent } from './dashboard/coberturas/embarcacion/embarcacion.component';
import { AutoDenunciaComponent } from './denuncia/denuncia/auto-denuncia/auto-denuncia.component';
import { HogarDenunciaComponent } from './denuncia/denuncia/hogar-denuncia/hogar-denuncia.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    LogInitialComponent,
    LogHomeComponent,
    DashboardComponent,
    HeaderComponent,
    MenuComponent,
    TitleComponent,
    SummaryComponent,
    ContentComponent,
    ActionComponent,
    NeedComponent,
    MoreComponent,
    MenuSlideComponent,
    FloatComponent,
    DenunciarComponent,
    HistorialComponent,
    ConsecuenciasComponent,
    DetallesComponent,
    TercerosComponent,
    VehiculosComponent,
    CosasComponent,
    TitularComponent,
    GuardarComponent,
    FinalizarComponent,
    ConsecuenciasDeRoboComponent,
    ConsecuenciasHogarComponent,
    DetallesHogarComponent,
    AutoComponent,
    HogarComponent,
    EmbarcacionComponent,
    AutoDenunciaComponent,
    HogarDenunciaComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PasswordModule,
    DialogModule,
    SplitButtonModule,
    ToastModule,
    TabViewModule,
    CodeHighlighterModule,
    BrowserAnimationsModule,
    AccordionModule,
    SelectButtonModule,
    ButtonModule,
    FieldsetModule,
    CarouselModule,
    DropdownModule,
    RadioButtonModule,
    MenuModule,
    MenubarModule,
    TableModule,
    InputSwitchModule,
    CheckboxModule,
    InputTextModule,
    PaginatorModule,
    TooltipModule,
    InputTextareaModule,
    KeyFilterModule,
    GMapModule,
    MessageModule,
    MessagesModule,
    PanelModule,
    CalendarModule
  ],
  providers: [
    RouterLinkActive,
    MessageService,
    PolizaService,
    CoverturaService,
    CoverturaMoreService,
    NeedService,
    HistorialService,
    IdentificarService,
    TerceroService,
    VehiculoService,
    CosaService,
    ConductorService,
    PolizaActivaService,
    PolizaBasicaService,
    PolizaBasicaINService,
    PolizaClasicaService,
    PolizaDiezService,
    PolizaPremiumCarService,
    PolizaTodoAutoService,
    PolizaUnoService,
    PolizaXpressCarService,
    PolizaBasicaMasMotoService,
    PolizaBasicaMotoService,
    PolizaPremiumMotoService,
    PolizaUnoMotoService,
    PolizaAbiertaHogarService,
    PolizaModuladaHogarService,
    PolizaTodoRiesgoHogarService,
    PolizaMascotaHogarService,
    PolizaCintaAzulService,
    PolizaCintaMarineraService,
    PolizaAPVidaService,
    PolizaAPPlatinoVidaService,
    PolizaAP24VidaService,
    PolizaAPDeportivoVidaService,
    PolizaPlusRVidaService,
    PolizaSepelioVidaService,
    PolizaSimpleVidaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
