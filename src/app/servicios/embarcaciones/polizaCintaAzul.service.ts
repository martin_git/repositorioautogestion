import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaCintaAzulService {

    private polizaCintaAzul:any[] = [
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaAzul", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños hacia personas transportadas y/o no transportadas. En exceso (y hasta el duplo) del valor asegurado de la embarcación.",
            icono: "assets/im_dashboard/embarcaciones/Resp_civil.png"
        },
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaAzul", 
            cobertura: "Perdida de la Embarcación",
            informacion: "En caso de robo total y/o parcial de la embarcación. O por daños causados por: naufragio, incendio, varamiento, colisión, rayo y/o explosión.",
            icono: "assets/im_dashboard/embarcaciones/Danos.png"
        },
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaAzul", 
            cobertura: "Incendio en Guarderia",
            informacion: "En caso de incendio en guardería.",
            icono: "assets/im_dashboard/embarcaciones/Incendio_guardia.png"
        },
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaAzul", 
            cobertura: "Bonificación",
            informacion: "En caso de no tener siniestralidad",
            icono: "assets/im_dashboard/embarcaciones/Bonificacion.png"
        },
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaAzul", 
            cobertura: "Asistencia Mecánica",
            informacion: "En caso de precisar remolque esta cobertura te brinda asistencia las 24 hs., los 365 días (para el Delta del Paraná, Carmelo, Nueva Palmira, Isla Martín García.",
            icono: "assets/im_dashboard/embarcaciones/Asistencia_mecanica.png"
        }
      ];
      
    
    constructor() {}

    getPolizaCintaAzul(){
        return this.polizaCintaAzul;
    }
}
interface PolizaCintaAzul{}