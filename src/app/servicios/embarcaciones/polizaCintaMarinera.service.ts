import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaCintaMarineraService {

    private polizaCintaMarinera:any[] = [
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaMarinera", 
            cobertura: "Responsabilidad Civil",
            informacion: "En caso de incendio hasta la suma asegurada de la embarcación. Incluidas las perosnas transoportdas y/o no transportadas",
            icono: "assets/im_dashboard/embarcaciones/Resp_Civil.png"
        },
        { 
            id: "embarcaciones",
            poliza: "Poliza CintaMarinera", 
            cobertura: "Robo",
            informacion: "En caso de pérdida y/o daño causados por: naufragio, incendio, varamiento, colisión, rayo y/o explosión.",
            icono: "assets/im_dashboard/embarcaciones/Robo.png"
        }
      ];
      
    
    constructor() {}

    getPolizaCintaMarinera(){
        return this.polizaCintaMarinera;
    }
}
interface PolizaCintaMarinera{}