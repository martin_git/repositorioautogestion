import { Injectable } from "@angular/core";

@Injectable ()
export class IdentificarService {

    private identificar:any[] = [
        { 
            nombre: "Cristal de techo", 
            img: "assets/im_dashboard/auto/Cristales.png",
            id: "cristalDeTecho"
        },
        { 
            nombre: "Cristales", 
            img: "assets/im_dashboard/auto/Cristales.png",
            id: "Cristales"
        },
        { 
            nombre: "Cerraduras", 
            img: "assets/im_dashboard/auto/cerradura.png",
            id: "cerraduras"
        },
        { 
            nombre: "Granizo", 
            img: "assets/im_dashboard/auto/Granizo.png",
            id: "granizo"
        }
      ];
      
    
    constructor() {}

    getIdentificar(){
        return this.identificar;
    }
}

interface Historial{
}