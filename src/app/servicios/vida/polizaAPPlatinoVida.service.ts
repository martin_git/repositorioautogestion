import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaAPPlatinoVidaService {

    private polizaAPPlatinoVida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza APPlatinoVida", 
            cobertura: "Muerte por Accidente",
            informacion: "En caso de eventual fallecimiento por casuda de una enfermedad.",
            icono: "assets/im_dashboard/vida/ap/Muerte_accidente.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APPlatinoVida", 
            cobertura: "Invalidez",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente o enfermedad.",
            icono: "assets/im_dashboard/vida/ap/Invalidez.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APPlatinoVida", 
            cobertura: "Acto Delictivo",
            informacion: "En caso de haber tenido un accidente por causa de un hecho delictivo, se le dará un incremento del 25% de aumento de la suma asegurada.",
            icono: "assets/im_dashboard/vida/ap/Robo.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APPlatinoVida", 
            cobertura: "Contención y Protección",
            informacion: "en caso de haber sufrido una situación traumática de robos en cajeros automáticos.",
            icono: "assets/im_dashboard/vida/ap/Contencion.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APPlatinoVida", 
            cobertura: "Traslado",
            informacion: "En caso de accidente se pone a dispocicón transporte o repatriación",
            icono: "assets/im_dashboard/vida/ap/Traslado_emergencia.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APPlatinoVida", 
            cobertura: "Mensajes Urgentes",
            informacion: "En caso de necesitar comunicación con familiares del asegurado.",
            icono: "assets/im_dashboard/vida/ap/Mensajes.png"
        }
      ];
      
    
    constructor() {}

    getPolizaAPPlatinoVida(){
        return this.polizaAPPlatinoVida;
    }
}
interface PolizaAPPlatinoVida{}