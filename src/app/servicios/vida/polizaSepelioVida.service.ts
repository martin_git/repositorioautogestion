import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaSepelioVidaService {

    private polizaSepelioVida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Traslado",
            informacion: "En caso de fallecimiento se pondra a disposición el traslado desde el lugar ocurrido.",
            icono: "assets/im_dashboard/vida/sepelio/Traslado_emergencia.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Sala Velatoria",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente enfermedad.",
            icono: "assets/im_dashboard/vida/sepelio/Sala_velatorio.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Servicio de Cafeteria",
            informacion: "En caso de solicitar asistencia médica, farmacéutica o por accidente se devolveré una suma correspondiente a dicho gasto.",
            icono: "assets/im_dashboard/vida/sepelio/Servicio_cafeteria.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Asistencia Médica",
            informacion: "Podrás contar con este servicio en todos los viajes que realices a países limítrofes y en el resto del mundo.",
            icono: "assets/im_dashboard/vida/sepelio/Asistencia_medica.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Carroza Fúnebre",
            informacion: "Transporte o repatriación sanitaria en caso de accidente con lesiones. También incluye el traslado del beneficiario fallecido a consecuencia de accidente.",
            icono: "assets/im_dashboard/vida/sepelio/Carroza_funebre.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Trámites",
            informacion: "En caso de accidente y ante riesgo de vida, las 24 Hs.",
            icono: "assets/im_dashboard/vida/sepelio/Tramites.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Certificado de Defunción",
            informacion: "En caso de necesitar comunicación con familiares del asegurado",
            icono: "assets/im_dashboard/vida/sepelio/Certificado_defuncion.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SepelioVida", 
            cobertura: "Reintegro de Gastos",
            informacion: "Los familiares del asegurado realizarán la contratación de las prestaciones necesarias de forma independiente y recibirán luego un reintegro por los gastos realizados, hasta la suma máxima establecida en la póliza.",
            icono: "assets/im_dashboard/vida/sepelio/Reintegro.png"
        }
      ];
      
    
    constructor() {}

    getPolizaSepelioVida(){
        return this.polizaSepelioVida;
    }
}
interface PolizaSepelioVida{}