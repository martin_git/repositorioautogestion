import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaSimpleVidaService {

    private polizaSimpleVida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza SimpleVida", 
            cobertura: "Muerte por Enfermedad",
            informacion: "En caso de eventual fallecimiento por casuda de una enfermedad.",
            icono: "assets/im_dashboard/vida/Muerte_por_enfermedad.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SimpleVida", 
            cobertura: "Indemnización",
            informacion: "Cubre el riesgo de muerte económicamente prematura e inoportuna, amparando y manteniendo la estabilidad económica del grupo familiar.",
            icono: "assets/im_dashboard/vida/Indemnizacion.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SimpleVida", 
            cobertura: "Invalidez",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente o enfermedad.",
            icono: "assets/im_dashboard/vida/Invalidez.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SimpleVida", 
            cobertura: "Traslado de Ambulancia",
            informacion: "En caso de accidente y ante riesgo de vida, las 24 Hs. todo el año, sin límite. ",
            icono: "assets/im_dashboard/vida/Traslado_ambulancia.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SimpleVida", 
            cobertura: "Asistencia al Viajero",
            informacion: "Ante eventual accidente fuera del país.",
            icono: "assets/im_dashboard/vida/Asistencia_viajero.png"
        },
        { 
            id: "vida",
            poliza: "Poliza SimpleVida", 
            cobertura: "Segunda Opinión",
            informacion: "En caso de fallecimiento se dará un monto por cada ocupante.",
            icono: "assets/im_dashboard/vida/Segunda_opinion.png"
        }
      ];
      
    
    constructor() {}

    getPolizaSimpleVida(){
        return this.polizaSimpleVida;
    }
}
interface PolizaSimpleVida{}