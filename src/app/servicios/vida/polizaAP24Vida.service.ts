import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaAP24VidaService {

    private polizaAP24Vida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza AP24Vida", 
            cobertura: "Muerte por Acciodente",
            informacion: "En caso de eventual fallecimiento por casuda de una enfermedad",
            icono: "assets/im_dashboard/vida/ap/Muerte_accidente.png"
        },
        { 
            id: "vida",
            poliza: "Poliza AP24Vida", 
            cobertura: "Invalidez",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente enfermedad.",
            icono: "assets/im_dashboard/vida/ap/Invalidez.png"
        },
        { 
            id: "vida",
            poliza: "Poliza AP24Vida", 
            cobertura: "Reintegro de Gastos",
            informacion: "En caso de solicitar asistencia médica, farmacéutica o por accidente se devolveré una suma correspondiente a dicho gasto.",
            icono: "assets/im_dashboard/vida/ap/Reintegro.png"
        }
      ];
      
    
    constructor() {}

    getPolizaAP24Vida(){
        return this.polizaAP24Vida;
    }
}
interface PolizaAP24Vida{}