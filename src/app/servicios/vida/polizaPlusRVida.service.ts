import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaPlusRVidaService {

    private polizaPlusRVida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza PlusRVida", 
            cobertura: "Muerte por Enfermedad",
            informacion: "En caso de eventual fallecimiento por casuda de una enfermedad.",
            icono: "assets/im_dashboard/vida/Muerte_por_enfermedad.png"
        },
        { 
            id: "vida",
            poliza: "Poliza PlusRVida", 
            cobertura: "Indemnización",
            informacion: "Cubre el riesgo de muerte económicamente prematura e inoportuna, amparando y manteniendo la estabilidad económica del grupo familiar.",
            icono: "assets/im_dashboard/vida/Indemnizacion.png"
        },
        { 
            id: "vida",
            poliza: "Poliza PlusRVida", 
            cobertura: "Invalidez",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente o enfermedad.",
            icono: "assets/im_dashboard/vida/Invalidez.png"
        }
      ];
      
    
    constructor() {}

    getPolizaPlusRVida(){
        return this.polizaPlusRVida;
    }
}
interface PolizaPlusRVida{}