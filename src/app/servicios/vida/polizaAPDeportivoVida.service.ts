import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaAPDeportivoVidaService {

    private polizaAPDeportivoVida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza APDeportivoVida", 
            cobertura: "Muerte en Ambito Laboral",
            informacion: "en caso de de accidente dentro de un espacio deportivo (competencias y entrenamientos).",
            icono: "assets/im_dashboard/vida/ap/Muerte_accidente.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APDeportivoVida", 
            cobertura: "Invalidez",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente o enfermedad.",
            icono: "assets/im_dashboard/vida/ap/Invalidez.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APDeportivoVida", 
            cobertura: "Reintegro de Gastos",
            informacion: "En caso de solicitar asistencia médica, farmacéutica o por accidente en el ámbito deportivo (competencias y entrenamientos) , se devolveré una suma correspondiente a dicho gasto.",
            icono: "assets/im_dashboard/vida/ap/Reintegro.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APDeportivoVida", 
            cobertura: "Accidente in itínere",
            informacion: "En caso de accidentes que pudieras sufrir durante el traslado tanto al lugar donde practicás tu deporte como al retorno a tu hogar, luego del mismo.",
            icono: "assets/im_dashboard/vida/ap/Accidente_itinere.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APDeportivoVida", 
            cobertura: "Asistencia Médica de Emergencia",
            informacion: "En caso de urgencias en el ámbito deportivo. El servicio consiste pura y exclusivamente en el traslado del asegurado desde el lugar en que se accidenta a causa de la práctica deportiva hasta el lugar donde éste decida ser atendido. El costo del mismo será deducido del límite máximo anual que incluye la asistencia médico farmacéutica.",
            icono: "assets/im_dashboard/vida/ap/Traslado_emergencia.png"
        }
      ];
      
    
    constructor() {}

    getPolizaAPDeportivoVida(){
        return this.polizaAPDeportivoVida;
    }
}
interface PolizaAPDeportivoVida{}