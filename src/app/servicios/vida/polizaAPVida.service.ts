import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaAPVidaService {

    private polizaAPVida:any[] = [
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Muerte por Acciodente",
            informacion: "En caso de eventual fallecimiento por casuda de una enfermedad",
            icono: "assets/im_dashboard/vida/ap/Muerte_accidente.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Invalidez",
            informacion: "En caso de nulidad o reducción por consecuencia de accidente enfermedad.",
            icono: "assets/im_dashboard/vida/ap/Invalidez.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Reintegro de Gastos",
            informacion: "En caso de solicitar asistencia médica, farmacéutica o por accidente se devolveré una suma correspondiente a dicho gasto.",
            icono: "assets/im_dashboard/vida/ap/Reintegro.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Cobertura en paises Limitrofes",
            informacion: "Podrás contar con este servicio en todos los viajes que realices a países limítrofes y en el resto del mundo.",
            icono: "assets/im_dashboard/vida/ap/Paises_limitrofes.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Traslado Sanitario",
            informacion: "Transporte o repatriación sanitaria en caso de accidente con lesiones. También incluye el traslado del beneficiario fallecido a consecuencia de accidente.",
            icono: "assets/im_dashboard/vida/ap/Traslado_emergencia.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Asistencia Sanitaria",
            informacion: "En caso de accidente y ante riesgo de vida, las 24 Hs.",
            icono: "assets/im_dashboard/vida/ap/Asistencia_sanitaria.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Mensajes Urgentes",
            informacion: "En caso de necesitar comunicación con familiares del asegurado",
            icono: "assets/im_dashboard/vida/ap/Mensajes.png"
        },
        { 
            id: "vida",
            poliza: "Poliza APVida", 
            cobertura: "Permanencia en el extranjero",
            informacion: "Beneficios válidos por 60 días",
            icono: "assets/im_dashboard/vida/ap/Permenencia_extranjero.png"
        }
      ];
      
    
    constructor() {}

    getPolizaAPVida(){
        return this.polizaAPVida;
    }
}
interface PolizaAPVida{}