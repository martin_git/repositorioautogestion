import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaService {

    private poliza:any[] = [
        {
          id: "auto",
          state: "b_state",
          nombre: "Auto",
          numero: "123456789",
          dominio: "AA 123 BB",
          estado: "Vigente",
          estadoactivo: "state",
          img: "assets/im_dashboard/polizas/auto.png"
        },
        {
          state:"b_medium",
          nombre: "hogar",
          numero: "123456789",
          dominio: "AA 123 BB",
          estado: "Vencida",
          estado_activo: "medium",
          img: "assets/im_dashboard/polizas/hogar.png"
        },
        {
          state:"b_medium",
          nombre: "Moto",
          numero: "123456789",
          dominio: "AA 123 BB",
          estado: "Vencida",
          estado_activo: "medium",
          img: "assets/im_dashboard/polizas/moto.png"
        },
        {
          state:"b_null",
          nombre: "Vida",
          numero: "123456789",
          dominio: "AA 123 BB",
          estado: "Anulada",
          estado_activo: "null",
          img: "assets/im_dashboard/polizas/vida.png"
        },
        {
          state:"b_medium",
          nombre: "Mascota",
          numero: "123456789",
          dominio: "AA 123 BB",
          estado: "Vencida",
          estado_activo: "medium",
          img: "assets/im_dashboard/polizas/mascota.png"
        },
        {
          state:"b_null",
          nombre: "Auto",
          numero: "123456789",
          dominio: "AA 123 BB",
          estado: "Anulada",
          estado_activo: "null",
          img: "assets/im_dashboard/polizas/auto.png"
        }
      ];
    
    constructor() {
        console.log( "Servicio listo para usar!!!!" );
        
    }

getPoliza(){
    return this.poliza;
}





}

interface Poliza{

}