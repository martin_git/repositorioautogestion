import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaTodoRiesgoHogarService {

    private polizaTodoRiesgoHogar:any[] = [
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños causados a terceros en el uso del vehículo",
            icono: "assets/im_dashboard/hogar/Resp_Civil.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Daños por Agua",
            informacion: "Acción directa del agua a muebles o inmuebles (sujeto a verificación y/o condiciones de cobertura)",
            icono: "assets/im_dashboard/hogar/Danos_por_agua.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Incendio Contenido",
            informacion: "A raíz de un incendio se produce daños en el contenido de tu domicilio (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Incendio_contenido.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Incendio de edificio",
            informacion: "A raíz de un incendio se produce daños en la edificación de tu domicilio (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Incendio_edificio.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Daños por causas Naturales",
            informacion: "Incluye:  rayo contenido, vendaval, ciclón, tornado, terremoto, derrumbe, granizo e innundación (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Danos_causas_naturales.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Asistencia ante emergencias",
            informacion: "En caso de necesitar asistencia de plomería, cerrajería, electricidad, cristales, asistencia legal y vigilancia.",
            icono: "assets/im_dashboard/hogar/Asistencia_emergencia.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Accidentes Personales",
            informacion: "Esta cobertura sirve en caso de tener lesiones físicas dentro de tu domicilio por un accidente (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Accidentes_personales.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Alimentos Refrigerados",
            informacion: "En caso de un corte del sumistro eléctrico por más de 12 horas continuas y sufris la perdida de alimentos (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Alimentos_refrigerados.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Robo y/o hurto",
            informacion: "En caso de sufrir faltantes de bienes dentro del hogar (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Robo_hurto.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Accidente al mobiliario",
            informacion: "En caso de sufrir rotura de mobiliario personal estando de viaje.",
            icono: "assets/im_dashboard/hogar/Accidentes_mobiliario.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Roturas de Cristales",
            informacion: "Ante un caso de accidente (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Rotura_cristales.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Electrodomesticos",
            informacion: "Rotura de electrodomésticos a causa de accidente.  (Sujeto a verificación y/o condiciones de cobertura). ",
            icono: "assets/im_dashboard/hogar/Electrodomesticos.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Todo Riesgo",
            informacion: "Sin franquicia. Incluye: parlante, TV, PC, microondas, heladeras, lavarropas, aire acondicionado y consolas de juego (excluyendo accesorios). (Sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Todo_riesgo.png"
        },
        { 
            id: "hogar",
            poliza: "Todo Riesgo", 
            cobertura: "Inundación",
            informacion: "Sin franquicia. Incluye: parlante, TV, PC, microondas, heladeras, lavarropas, aire acondicionado y consolas de juego (excluyendo accesorios). (Sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Danos_inundacion.png"
        }
      ];
      
    
    constructor() {}

    getPolizaTodoRiesgoHogar(){
        return this.polizaTodoRiesgoHogar;
    }
}
interface PolizaTodoRiesgoHogar{}