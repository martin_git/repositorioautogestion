import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaModuladaHogarService {

    private polizaModuladaHogar:any[] = [
        { 
            id: "hogar",
            poliza: "Poliza ModuladaHogar", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños causados a terceros en el uso del vehículo",
            icono: "assets/im_dashboard/hogar/Resp_Civil.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza ModuladaHogar", 
            cobertura: "Daños por Agua",
            informacion: "Acción directa del agua a muebles o inmuebles (sujeto a verificación y/o condiciones de cobertura)",
            icono: "assets/im_dashboard/hogar/Danos_por_agua.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza ModuladaHogar", 
            cobertura: "Incendio Contenido",
            informacion: "A raíz de un incendio se produce daños en el contenido de tu domicilio (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Incendio_contenido.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza ModuladaHogar", 
            cobertura: "Asistencia ante emergencias",
            informacion: "En caso de necesitar asistencia de plomería, cerrajería, electricidad, cristales, asistencia legal y vigilancia.",
            icono: "assets/im_dashboard/hogar/Asistencia_emergencia.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza ModuladaHogar", 
            cobertura: "Inundación",
            informacion: "Sin franquicia. Incluye: parlante, TV, PC, microondas, heladeras, lavarropas, aire acondicionado y consolas de juego (excluyendo accesorios). (Sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Danos_inundacion.png"
        }
      ];
      
    
    constructor() {}

    getPolizaModuladaHogar(){
        return this.polizaModuladaHogar;
    }
}
interface PolizaModuladaHogar{}