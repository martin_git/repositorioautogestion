import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaMascotaHogarService {

    private polizaMascotaHogar:any[] = [
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Asistencia Veterinaria",
            informacion: "Con esta cobertura se indemnizan los gastos de asistencia veterinaria, a causa de un accidente que padeciera el animal asegurado.",
            icono: "assets/im_dashboard/mascota/Asistencia_veterinaria.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Examenes Clínicos",
            informacion: "En caso de tener que realizarle a la mascota radiografías, análisis, electrocardiogramas y ecografías",
            icono: "assets/im_dashboard/mascota/Examenes_clinicos.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Intervenciones quirurjicas",
            informacion: "En caso de eventual operación esta cobertura te cubre de la anestesia, material quirúrgico, medicamentos y prótesis.",
            icono: "assets/im_dashboard/mascota/Intervenciones_quirurgicas.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Cuidados Postoperatorios",
            informacion: "En caso de haber tenido una intervención quirúrgica, esta cobertura te brinda cura y estadía en clínica, cuando fuera necesario.",
            icono: "assets/im_dashboard/mascota/Cuidado_postoperatorio.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Robo",
            informacion: "En caso de robo de la mascota, la indemnización será del valor de un cachorro de la raza del animal asegurado (con límite de la suma asegurada establecida).",
            icono: "assets/im_dashboard/mascota/Robo.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Extravío",
            informacion: " En caso de pérdida del animal por descuido del asegurado o persona encargada de su custodia (paseadores, residencias o personas a cargo del animal), se cubrirán los gastos relacionados con su búsqueda.",
            icono: "assets/im_dashboard/mascota/Extravio.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Estadía en residencia",
            informacion: "Esta cobertura se otorga exclusivamente si el dueño de la mascota es hospitalizado. ",
            icono: "assets/im_dashboard/mascota/estadia_residencia.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Idemnización por Muerte",
            informacion: "En caso de poner fin a un sufrimiento irreversible del animal, como consecuencia de un accidente, vejez o enfermedad.",
            icono: "assets/im_dashboard/mascota/Idemnizacion_muerte.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza MascotaHogar", 
            cobertura: "Responsabilidad Civil",
            informacion: "Rotura de electrodomésticos a causa de accidente.  (Sujeto a verificación y/o condiciones de cobertura). ",
            icono: "assets/im_dashboard/mascota/Resp_civil.png"
        },
      ];
      
    
    constructor() {}

    getPolizaMascotaHogar(){
        return this.polizaMascotaHogar;
    }
}
interface PolizaMascotaHogar{}