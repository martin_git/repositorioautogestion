import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaAbiertaHogarService {

    private polizaAbiertaHogar:any[] = [
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños causados a terceros en el uso del vehículo",
            icono: "assets/im_dashboard/hogar/Resp_Civil.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Daños por Agua",
            informacion: "Acción directa del agua a muebles o inmuebles (sujeto a verificación y/o condiciones de cobertura)",
            icono: "assets/im_dashboard/hogar/Danos_por_agua.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Incendio Contenido",
            informacion: "A raíz de un incendio se produce daños en el contenido de tu domicilio (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Incendio_contenido.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Incendio de edificio",
            informacion: "A raíz de un incendio se produce daños en la edificación de tu domicilio (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Incendio_edificio.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Daños por causas Naturales",
            informacion: "Incluye:  rayo contenido, vendaval, ciclón, tornado, terremoto, derrumbe, granizo e innundación (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Danos_por_causas_naturales.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Asistencia ante emergencias",
            informacion: "En caso de necesitar asistencia de plomería, cerrajería, electricidad, cristales, asistencia legal y vigilancia.",
            icono: "assets/im_dashboard/hogar/Asistencia_emergencia.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Robo y/o hurto",
            informacion: "En caso de sufrir faltantes de bienes dentro del hogar (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Robo_hurto.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Roturas de Cristales",
            informacion: "Ante un caso de accidente (sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Rotura_Cristales.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Electrodomesticos",
            informacion: "Rotura de electrodomésticos a causa de accidente.  (Sujeto a verificación y/o condiciones de cobertura). ",
            icono: "assets/im_dashboard/hogar/Electrodomesticos.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Inundación",
            informacion: "Sin franquicia. Incluye: parlante, TV, PC, microondas, heladeras, lavarropas, aire acondicionado y consolas de juego (excluyendo accesorios). (Sujeto a verificación y/o condiciones de cobertura).",
            icono: "assets/im_dashboard/hogar/Danos_inundacion.png"
        },
        { 
            id: "hogar",
            poliza: "Poliza AbiertaHogar", 
            cobertura: "Bienes fuera del Hogar",
            informacion: "Incluye: bicicletas, cámaras fotográficas, reproductores de música, filmadoras, tablets y notebooks.",
            icono: "assets/im_dashboard/hogar/Bienes_fuera_hogar.png"
        }
      ];
      
    
    constructor() {}

    getPolizaAbiertaHogar(){
        return this.polizaAbiertaHogar;
    }
}
interface PolizaAbiertaHogar{}