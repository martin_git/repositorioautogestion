import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaUnoService {

    private polizaUno:any[] = [
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños causados a terceros en el uso del vehículo",
            icono: "assets/im_dashboard/auto/Resp_Civil.png"
        },
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Accidentes Personales",
            informacion: "En caso de fallecimiento se dará un monto por cada ocupante.",
            icono: "assets/im_dashboard/auto/Accidentes_personales.png"
        },
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Asistencia al Vehículo",
            informacion: "Cobertura en Argentina y países limítrofes. Incluye mecánica de urgencia y remolque.",
            icono: "assets/im_dashboard/auto/Asistencia_al_vehiculo.png"
        },
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Defensa Penal",
            informacion: "MAPFRE se hace cargo de tu defensa desde las primeras instancias hasta el fin del proceso. Si elegís la defensa de otro profesional, MAPFRE participa en un porcentaje de dinero estipulado en tu cobertura.",
            icono: "assets/im_dashboard/auto/Defensa_personal.png"
        },
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Asesoramiento Legal",
            informacion: "Contarás con asistencia personalizada legal ante un siniestro en el cual se haya producido alguna lesión, servicio de asistencia y remolque de MAPFRE.",
            icono: "assets/im_dashboard/auto/Asesoramiento_legal.png"
        },
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Extensión de cobertura",
            informacion: "Cobertura en países limítrofes: Uruguay, Chile, Paraguay, Brasil y Bolivia. Incluye mecánica de urgencia y remolque.",
            icono: "assets/im_dashboard/auto/Extension_de_cobertura"
        }
      ];
      
    
    constructor() {}

    getPolizaUno(){
        return this.polizaUno;
    }
}
interface PolizaUno{}