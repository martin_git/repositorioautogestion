import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaTodoAutoService {

    private polizaTodoAuto:any[] = [
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños causados a terceros en el uso del vehículo",
            icono: "assets/im_dashboard/auto/Resp_Civil.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Accidentes Personales",
            informacion: "En caso de fallecimiento se dará un monto por cada ocupante.",
            icono: "assets/im_dashboard/auto/Accidentes_personales.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Asistencia al Vehículo",
            informacion: "Cobertura en Argentina y países limítrofes. Incluye mecánica de urgencia y remolque.",
            icono: "assets/im_dashboard/auto/Asistencia_al_vehiculo.png"
        },
        { 
            id: "auto",
            poliza: "poliza uno", 
            cobertura: "Defensa Penal",
            informacion: "MAPFRE se hace cargo de tu defensa desde las primeras instancias hasta el fin del proceso. Si elegís la defensa de otro profesional, MAPFRE participa en un porcentaje de dinero estipulado en tu cobertura.",
            icono: "assets/im_dashboard/auto/Defensa_personal.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Asesoramiento Legal",
            informacion: "Contarás con asistencia personalizada legal ante un siniestro en el cual se haya producido alguna lesión, servicio de asistencia y remolque de MAPFRE.",
            icono: "assets/im_dashboard/auto/Asesoramiento_legal.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Extensión de cobertura",
            informacion: "Cobertura en países limítrofes: Uruguay, Chile, Paraguay, Brasil y Bolivia. Incluye mecánica de urgencia y remolque.",
            icono: "assets/im_dashboard/auto/Extension_de_cobertura.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Robo y/o hurto",
            informacion: "En caso de  pérdida total, MAPFRE abonará el valor del vehículo asegurado hasta el 100% de la suma asegurada, sin ninguna franquicia.",
            icono: "assets/im_dashboard/auto/robo_hurto.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Incendio",
            informacion: "La pérdida total de la unidad será abonado sin ninguna franquicia a tu cargo.",
            icono: "assets/im_dashboard/auto/Incendio.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Destrucciòn total",
            informacion: "Será considerado destrucción total cuando la reparación supere el 80% del valor asegurado y se abonará hasta el total de la suma asegurada en póliza, sin ninguna franquicia o monto a su cargo.",
            icono: "assets/im_dashboard/auto/Destruccion_total.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Roturas de cristales",
            informacion: "En caso de dañarse los cristales laterales, luneta y/o parabrisa por cualquier causa sin franquicia.",
            icono: "assets/im_dashboard/auto/Crsitales.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Roturas de cerradura",
            informacion: "Seguro por cualquier causa sin franquicia, con un límite del 3% de la suma asegurada del vehículo por año de vigencia.",
            icono: "assets/im_dashboard/auto/Cerradura.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Granizo",
            informacion: "Monto hasta $ 10.000.000.",
            icono: "assets/im_dashboard/auto/Granizo.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Daños por Inundación",
            informacion: "Seguro por cualquier causa sin franquicia, con un límite del 3% de la suma asegurada del vehículo por año de vigencia.",
            icono: "assets/im_dashboard/auto/Inundacion.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Daños Parciales",
            informacion: "Por causa de accidente: con franquicia fija del 5% de la suma asegurada del vehículo por año de vigencia al momento del siniestro.",
            icono: "assets/im_dashboard/auto/Danos_parciales.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Destrucción Total",
            informacion: "Será considerado destrucción total cuando la reparación supere el 80% del valor asegurado y se abonará hasta el total de la suma asegurada en póliza, sin ninguna franquicia o monto a su cargo.",
            icono: "assets/im_dashboard/auto/Destruccion_total.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Reposición de cubiertas",
            informacion: "Se repondrán a nuevo por robo / hurto durante los 2 (dos) primeros años de la fecha de factura de compra del 0KM.",
            icono: "assets/im_dashboard/auto/Reposicion_de_cubiertas.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto", 
            cobertura: "Reposición de cubiertas",
            informacion: "Durante el primer año de cobertura se repondrá el vehículo.",
            icono: "assets/im_dashboard/auto/Reposicion_unidad.png"
        },
        { 
            id: "auto",
            poliza: "Poliza TodoAuto",
            cobertura: "Beneficio Mapfre vs. Mapfre",
            informacion: "Si chocás contra otro asegurado de MAPFRE ambos se benefician (el damnificado por la responsabilidad civil del culpable y el culpable por daños parciales hasta el límite establecido en la póliza).",
            icono: "assets/im_dashboard/auto/Mapfre_vs_Mapfre.png"
        }
      ];
      
    
    constructor() {}

    getPolizaTodoAuto(){
        return this.polizaTodoAuto;
    }
}
interface PolizaTodoAuto{}