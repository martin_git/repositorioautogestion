import { Injectable } from "@angular/core";

@Injectable ()
export class CoverturaMoreService {

    private coverturaMore:any[] = [
        { 
          poliza: "auto", 
          nombre: "Defensa Penal",
          detalle: "Monto hasta $10.000 para las categorias de autos, camionetas, vehículos remolcados y casas rodantes. Para camiones, semitracciones, acoplados, maquinarias rurales, la suma asciende a $18.000.",
          img: "assets/im_dashboard/auto/Defensa_personal.png"
        },
        {
          poliza: "moto",  
          nombre: "Asistencia al vehículo",
          detalle: "Cobertura en Argentina y países limítrofes. Incluye mecánica de urgencia y remolque.",
          img: "assets/im_dashboard/auto/Asistencia_al_vehiculo.png"
        },
        {
          poliza: "moto",
          nombre: "Granizo",
          detalle: "Cobertura en países limitrofes: Uruguay, Chile, Paraguay, Brasil y Bolivia",
          img: "assets/im_dashboard/auto/Granizo.png"
        },
        {
          poliza: "moto",
          nombre: "Rotura de cerradura",
          detalle: "Seguro por cualquier causa sin franquicia, con un límite del 3% de la suma asegurada del vehículo por año de vigencia.",
          img: "assets/im_dashboard/auto/cerradura.png"
        },
        {
          poliza: "hogar",   
          nombre: "Rotura de cristales",
          detalle: "Seguro por cualquier causa de rotura sin franquicia (con un límite del 3% de la suma asegurada. En la provincia de Tierra del Fuego, se aplica un límite anual de $1.000).",
          img: "assets/im_dashboard/auto/Cristales.png"
        },
        {
          poliza: "hogar", 
          nombre: "Destrucción total",
          detalle: "Será considerado destrucción total cuando la reparación supere el 80% del valor asegurado y se abonará hasta el total de la suma asegurada en póliza, sin ninguna franquicia o monto a su cargo.",
          img: "assets/im_dashboard/auto/Destrucción_total.png"
        },
        {
          poliza: "hogar",   
          nombre: "Defensa personal",
          detalle: "Seguro por cualquier causa sin franquicia, con un límite del 3% de la suma asegurada del vehículo por año de vigencia.",
          img: "assets/im_dashboard/auto/Defensa_personal.png"
        },
        {
          poliza: "hogar", 
          nombre: "Reposición del la unidad",
          detalle: "Seguro por cualquier causa de rotura sin franquicia (con un límite del 3% de la suma asegurada. En la provincia de Tierra del Fuego, se aplica un límite anual de $1.000).",
          img: "assets/im_dashboard/auto/Reposicion_unidad.png"
        },
        {
          poliza: "hogar",   
          nombre: "Inundación",
          detalle: "Será considerado destrucción total cuando la reparación supere el 80% del valor asegurado y se abonará hasta el total de la suma asegurada en póliza, sin ninguna franquicia o monto a su cargo.",
          img: "assets/im_dashboard/auto/Inundacion.png"
        },
        {
          poliza: "hogar", 
          nombre: "Reposición de cubiertas",
          detalle: "En caso de fallecimiento se dará un monto de $2500 por cada ocupante.",
          img: "assets/im_dashboard/auto/Reposicion_de_cubiertas.png"
        },
        {
          poliza: "hogar",   
          nombre: "Beneficio Mapfre vs Mapfre",
          detalle: "La pérdida total de la unidad será abonado sin ninguna franquicia a tu cargo.",
          img: "assets/im_dashboard/auto/Beneficio _MAPFRE_vs_MAPFRE.png"
        },
        {
          poliza: "hogar", 
          nombre: "Daños parciales",
          detalle: "En caso de fallecimiento se dará un monto de $2500 por cada ocupante.",
          img: "assets/im_dashboard/auto/Daños_parciales.png"
        },
        {
          poliza: "hogar", 
          nombre: "Reposición de cubiertas",
          detalle: "En caso de fallecimiento se dará un monto de $2500 por cada ocupante.",
          img: "assets/im_dashboard/auto/Reposicion_de_cubiertas.png"
        }
      ];
      
    
    constructor() {
        console.log( "COVERTURA listo para usar!!!!" );
        
    }

getCoverturaMore(){
    return this.coverturaMore;
}





}

interface Covertura{

}