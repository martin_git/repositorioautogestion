import { Injectable } from "@angular/core";

@Injectable ()
export class CoverturaService {

    private covertura:any[] = [
        { 
          id: "auto",
          poliza: "poliza uno", 
          cobertura: "Responsabilidad Civil",
          informacion: "Monto hasta $10.000 para las categorias de autos, camionetas, vehículos remolcados y casas rodantes. Para camiones, semitracciones, acoplados, maquinarias rurales, la suma asciende a $18.000.",
          icono: "assets/im_dashboard/auto/Resp_Civil.png"
        },
        {
          poliza: "moto",  
          nombre: "Extención de cobertura",
          detalle: "Cobertura en países limitrofes: Uruguay, Chile, Paraguay, Brasil y Bolivia",
          img: "assets/im_dashboard/auto/Extension_de_cobertura.png"
        },
        {
          poliza: "moto",
          nombre: "Asesoramiento legal",
          detalle: "Cobertura en países limitrofes: Uruguay, Chile, Paraguay, Brasil y Bolivia",
          img: "assets/im_dashboard/auto/Asesoramiento_legal.png"
        },
        {
          poliza: "moto",
          nombre: "Robo y/o hurto",
          detalle: "La pérdida total de la unidad será abonado sin ninguna franquicia a tu cargo.",
          img: "assets/im_dashboard/auto/robo_hurto.png"
        },
        {
          poliza: "hogar",   
          nombre: "Incendio",
          detalle: "La pérdida total de la unidad será abonado sin ninguna franquicia a tu cargo.",
          img: "assets/im_dashboard/auto/Incendio.png"
        },
        {
          poliza: "hogar", 
          nombre: "Accidentes personales",
          detalle: "En caso de fallecimiento se dará un monto de $2500 por cada ocupante.",
          img: "assets/im_dashboard/auto/Accidentes_personales.png"
        }
      ];
      
    
    constructor() {
        console.log( "COVERTURA listo para usar!!!!" );
        
    }

getCovertura(){
    return this.covertura;
}





}

interface Covertura{

}