import { Injectable } from "@angular/core";

@Injectable ()
export class PolizaPremiumMotoService {

    private polizaPremiumMoto:any[] = [
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Responsabilidad Civil",
            informacion: "Asegurar los daños causados a terceros en el uso del vehículo",
            icono: "assets/im_dashboard/moto/Resp_Civil.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Accidentes Personales",
            informacion: "En caso de fallecimiento se dará un monto de $2500 por cada ocupante.",
            icono: "assets/im_dashboard/moto/Accidentes_personales.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Asistencia al Vehículo",
            informacion: "Cobertura en Argentina y países limítrofes. Incluye mecánica de urgencia y remolque.",
            icono: "assets/im_dashboard/moto/Asistencia_en_viaje_24.png"
        },
        { 
            id: "moto",
            poliza: "poliza uno", 
            cobertura: "Defensa Penal",
            informacion: "MAPFRE se hace cargo de tu defensa desde las primeras instancias hasta el fin del proceso. Si elegís la defensa de otro profesional, MAPFRE participa en un porcentaje de dinero estipulado en tu cobertura.",
            icono: "assets/im_dashboard/moto/Defensa_personal.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Asesoramiento Legal",
            informacion: "Contarás con asistencia personalizada legal ante un siniestro en el cual se haya producido alguna lesión, servicio de asistencia y remolque de MAPFRE.",
            icono: "assets/im_dashboard/moto/Asesoramiento_legal.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Extensión de cobertura",
            informacion: "Cobertura en países limítrofes: Uruguay, Chile, Paraguay, Brasil y Bolivia.",
            icono: "assets/im_dashboard/moto/Extension_de_cobertura.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Robo y/o hurto",
            informacion: "En caso de  pérdida total, MAPFRE abonará el valor del vehículo asegurado hasta el 100% de la suma asegurada, sin ninguna franquicia.",
            icono: "assets/im_dashboard/moto/Robo_hurto.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Incendio",
            informacion: "La pérdida total de la unidad será abonado sin ninguna franquicia a tu cargo.",
            icono: "assets/im_dashboard/moto/Incendio.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Destrucciòn total",
            informacion: "Será considerado destrucción total cuando la reparación supere el 80% del valor asegurado y se abonará hasta el total de la suma asegurada en póliza, sin ninguna franquicia o monto a su cargo.",
            icono: "assets/im_dashboard/moto/Destruccion_total.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Daños Parciales",
            informacion: "Por accidente al amparo del robo y/o hurto total sin franquicia. Con franquicia variable del 5% del valor de la unidad con mínimo de $3.000. Para motos de alta gama franquicia del 5% con mínimo de $ 10.000.",
            icono: "assets/im_dashboard/moto/Danos_parciales.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Reposición de la unidad",
            informacion: "Durante el primer año de cobertura se repondrá el vehículo en caso de robo/hurto, incendio o accidente total.",
            icono: "assets/im_dashboard/moto/Reposicion_de_la_unidad.png"
        },
        { 
            id: "moto",
            poliza: "Poliza PremiumMoto", 
            cobertura: "Localización",
            informacion: "Seguro por cualquier causa de rotura sin franquicia (con un límite del 3% de la suma asegurada. En la provincia de Tierra del Fuego, se aplica un límite anual de $1.000).",
            icono: "assets/im_dashboard/moto/Localizacion.png"
        }
      ];
      
    
    constructor() {}

    getPolizaPremiumMoto(){
        return this.polizaPremiumMoto;
    }
}
interface PolizaPremiumMoto{}