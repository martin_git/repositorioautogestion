import { Injectable } from "@angular/core";

@Injectable ()
export class VehiculoService {

    private vehiculo:any[] = [
        { 
            marca: "Chevrolet", 
            modelo: "Gabriel Batistuta",
            patente: "Conductor del vehiculo contrario"
        },
        { 
            marca: "Nissan", 
            modelo: "Gabriel Batistuta",
            patente: "Conductor del vehiculo contrario"
        },
        { 
            marca: "Renault", 
            modelo: "Gabriel Batistuta",
            patente: "Conductor del vehiculo contrario"
        }
      ];
      
    
    constructor() {
    }

getVehiculo(){
    return this.vehiculo;
}

}

interface Vehiculo{

}