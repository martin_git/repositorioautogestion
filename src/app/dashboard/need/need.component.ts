import { Component, OnInit } from '@angular/core';
import { NeedService } from '../../servicios/need.service';

@Component({
  selector: 'app-need',
  templateUrl: './need.component.html',
  styleUrls: ['./need.component.css']
})
export class NeedComponent implements OnInit {

  need: any[];
  
  constructor( private _needService:NeedService ) {
    console.log("constructor");
}

  ngOnInit() {

    this.need = this._needService.getNeed();

    console.log(this.need);


  }

}
