import { Component, OnInit } from '@angular/core';
import { PolizaService } from '../../servicios/poliza.service';


@Component({
  selector: 'app-menu-slide',
  templateUrl: './menu-slide.component.html',
  styleUrls: ['./menu-slide.component.css']
})
export class MenuSlideComponent implements OnInit {

  poliza: any[];
  
  constructor( private _polizaService:PolizaService ) {
    console.log("constructor");
}

  ngOnInit() {

    this.poliza = this._polizaService.getPoliza();

    console.log(this.poliza);


  }

}