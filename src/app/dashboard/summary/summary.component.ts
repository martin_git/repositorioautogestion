import { Component, OnInit } from '@angular/core';

interface Ries {
  name: string;
  code: string;
}

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  collapse= false;

  collapse1= true;

  collapse2= false;
  
  collapse3= false;

  DropDescargas= false;

  DropSiniestro= false;

  content= true;

  denuncia= false;

  riesgos: Ries[];

  selectedRies: Ries;

  constructor() {


    this.riesgos = [
      {name: 'Toyota Corolla modelo 2019 versión 1 Pack electrico', code: 'LC'},
      {name: 'Renaul Sandero modelo 2020 versión 1 Pack electrico', code: 'LE'},
      {name: 'Chevrolet Corsa modelo 2016 versión 1 Pack electrico', code: 'CI'},
      {name: 'Fiat Toro modelo 2016 versión 1 Pack electrico', code: 'DNI'},
      {name: 'Renault Logan corolla modelo 2012 versión 1 Pack electrico', code: 'CUIT'}
  ];
    
   }

  ngOnInit() {
  }

}
