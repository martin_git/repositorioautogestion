import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {

  items: any [];
  message: MenuItem[];

  constructor() {}
 
  ngOnInit() {
    this.items = [
        {
          icon: 'pi pi-exclamation-triangle',  
          label: '¿Te choco un asegurado Mapfre?',
        },
        {
          icon: 'pi pi-info-circle',
          label: 'Atención al cliente',
          items: [
              {icon: 'pi pi-question-circle', label: 'Lineas de atención' },
              {icon: 'pi pi-question-circle', label: 'Todo lo que necesitas saber sobre la denuncia de un siniestro' },
              {icon: 'pi pi-question-circle', label: 'Oficinas Mapfre' },
              {icon: 'pi pi-question-circle', label: 'Talleres preferenciales' },
              {icon: 'pi pi-question-circle', label: 'Medios y lugares de pagos' },
              {icon: 'pi pi-question-circle', label: 'Condicionados de pólizas' },
              {icon: 'pi pi-question-circle', label: 'Preguntas frecuentes' }
          ]
        },
        {
            icon: 'pi pi-comment',
            label: 'Notificaciones',
            items: [
                {icon: 'pi pi-bell', label: 'Notificacion 1'},
                {icon: 'pi pi-bell', label: 'Notificacion 2'},
                {icon: 'pi pi-bell', label: 'Notificacion 3'},
                {icon: 'pi pi-bell', label: 'Notificacion 4'},
                {icon: 'pi pi-bell', label: 'Notificacion 5'}
            ]
        },
        {
          icon: 'pi pi-user',
          label: 'Mi perfil',
          items: [
            {label: 'Modificación de contraseña'},
            {label: 'Modificación de E-mail'},
            {label: 'Autorización de usuarios'},
            {label: 'DDJJ Persona expuesta politicamente'},
            {label: 'DDJJ Sujeto Obligado'}
          ]
      },
        {
          icon: 'pi pi-power-off',  
          label: 'Cerrar Sesión',
        }
    ];
}

}
