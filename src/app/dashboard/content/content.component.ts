import { Component, OnInit } from '@angular/core';
import { CoverturaService } from '../../servicios/covertura.service';
import { CoverturaMoreService } from '../../servicios/coverturaMore.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
   
  
    covertura:any[] = [];
    coverturaMore:any[] = [];


    constructor( private _coverturaService:CoverturaService,
                 private _coverturaMoreService:CoverturaMoreService) {
      console.log("constructor");
      
}

    ngOnInit() {

      this.covertura = this._coverturaService.getCovertura();

      console.log(this.covertura);

      this.coverturaMore = this._coverturaMoreService.getCoverturaMore();

      console.log(this.coverturaMore);


    }

}