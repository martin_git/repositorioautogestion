import { Component, OnInit } from '@angular/core';
import { PolizaDiezService } from '../../../servicios/auto/polizaDiez.service';

interface Ries {
  name: string;
  code: string;
}

@Component({
  selector: 'app-auto',
  templateUrl: './auto.component.html',
  styleUrls: ['./auto.component.css']
})
export class AutoComponent implements OnInit {

  poliza = "Auto";

  tipo = "Poliza 10";

  polizaDiez:any[] = [];

  collapse= false;

  date_open= true;

  collapse1= true;

  date_open1= true;

  collapse2= false;

  date_open2= false;
  
  collapse3= false;

  date_open3= true;

  DropDescargas= false;

  DropSiniestro= false;

  content= true;

  denuncia= false;

  check_expuesta= true;

  input_expuesta= false;

  resumen= true;

  resumen_denuncia= false;

  riesgos: Ries[];

  selectedRies: Ries;

  es: any;

  constructor(private _polizaDiezService:PolizaDiezService) {


    this.riesgos = [
      {name: 'Toyota Corolla modelo 2019 versión 1 Pack electrico', code: 'LC'},
      {name: 'Renaul Sandero modelo 2020 versión 1 Pack electrico', code: 'LE'},
      {name: 'Chevrolet Corsa modelo 2016 versión 1 Pack electrico', code: 'CI'},
      {name: 'Fiat Toro modelo 2016 versión 1 Pack electrico', code: 'DNI'},
      {name: 'Renault Logan corolla modelo 2012 versión 1 Pack electrico', code: 'CUIT'}
  ];
    
   }

  ngOnInit() {

    this.polizaDiez = this._polizaDiezService.getPolizaDiez();

    console.log(this.polizaDiez);

    this.es = {
      firstDayOfWeek: 1,
      dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
      dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
      dayNamesMin: [ "D","L","M","X","J","V","S" ],
      monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
      monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
      today: 'Hoy',
      clear: 'Borrar'
  }

  }

}

