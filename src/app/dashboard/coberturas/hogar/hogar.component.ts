import { Component, OnInit } from '@angular/core';
import { PolizaTodoRiesgoHogarService } from '../../../servicios/hogar/polizaTodoRiesgoHogar.service';

interface Ries {
  name: string;
  code: string;
}

@Component({
  selector: 'app-hogar',
  templateUrl: './hogar.component.html',
  styleUrls: ['./hogar.component.css']
})
export class HogarComponent implements OnInit {

  poliza = "Hogar";

  tipo = "Todo Riesgo";

  polizaTodoRiesgoHogar:any[] = [];

  collapse= false;

  date_open= true;

  collapse1= true;

  date_open1= true;

  collapse2= false;

  date_open2= false;
  
  collapse3= false;

  date_open3= true;

  DropDescargas= false;

  DropSiniestro= false;

  content= true;

  denuncia= false;

  check_expuesta= true;

  input_expuesta= false;

  resumen= true;

  resumen_denuncia= false;

  riesgos: Ries[];

  selectedRies: Ries;

  es: any;

  constructor(private _polizaTodoRiesgoHogarService:PolizaTodoRiesgoHogarService) {


    this.riesgos = [
      {name: 'Toyota Corolla modelo 2019 versión 1 Pack electrico', code: 'LC'},
      {name: 'Renaul Sandero modelo 2020 versión 1 Pack electrico', code: 'LE'},
      {name: 'Chevrolet Corsa modelo 2016 versión 1 Pack electrico', code: 'CI'},
      {name: 'Fiat Toro modelo 2016 versión 1 Pack electrico', code: 'DNI'},
      {name: 'Renault Logan corolla modelo 2012 versión 1 Pack electrico', code: 'CUIT'}
  ];
    
   }

  ngOnInit() {

    this.polizaTodoRiesgoHogar = this._polizaTodoRiesgoHogarService.getPolizaTodoRiesgoHogar();

    console.log(this.polizaTodoRiesgoHogar);

    this.es = {
      firstDayOfWeek: 1,
      dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
      dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
      dayNamesMin: [ "D","L","M","X","J","V","S" ],
      monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
      monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
      today: 'Hoy',
      clear: 'Borrar'
  }


  }

}

