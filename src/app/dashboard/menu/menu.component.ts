import { Component, OnInit } from '@angular/core';
import { PolizaService } from '../../servicios/poliza.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  poliza:any[] = [];

  collapsed= false;
  panelOpenState = false;


  constructor( private _polizaService:PolizaService ) {
    console.log("constructor");
}

  ngOnInit() {

    this.poliza = this._polizaService.getPoliza();

    console.log(this.poliza);


  }

  
}
