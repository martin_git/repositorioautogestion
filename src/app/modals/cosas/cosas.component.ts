import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-cosas',
  templateUrl: './cosas.component.html',
  styleUrls: ['./cosas.component.css'],
  providers: [MessageService]
})

export class CosasComponent implements OnInit {

  uservehi: FormGroup; 

  userform: FormGroup;

  submitted: boolean;

  aseguradoras: SelectItem[];

  constructor(private fb: FormBuilder, 
    private messageService: MessageService) { }

  ngOnInit() {

    this.userform = this.fb.group({
      'name': new FormControl('', Validators.required),
      'doc': new FormControl('', Validators.required),
      'docConductor': new FormControl('', Validators.required),
      'tel': new FormControl('', Validators.required),
      'telConductor': new FormControl('', Validators.required),
      'mail': new FormControl('', Validators.required),
      'dom': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.required),
      'poliza': new FormControl('', Validators.required),
      'aseguradora': new FormControl('', Validators.required)
    });

    this.aseguradoras = [];
    this.aseguradoras.push({label:'Selecciona Aseguradora', value:'Seleccion'});
    this.aseguradoras.push({label:'Mapfre', value:'SM'});
    this.aseguradoras.push({label:'Unigo', value:'VL'});
    this.aseguradoras.push({label:'La Nueva', value:'SF'});
    this.aseguradoras.push({label:'San Cristobal', value:'CF'});



  }

  onSubmit(value: string) {
    this.submitted = true;
    this.messageService.add({severity:'info', summary:'Success', detail:'Form Submitted'});
  }

  get diagnostic() { return JSON.stringify(this.userform.value); }

}
