import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-titular',
  templateUrl: './titular.component.html',
  styleUrls: ['./titular.component.css']
})
export class TitularComponent implements OnInit {

  uservehi: FormGroup; 

  userform: FormGroup;

  submitted: boolean;

  clases: SelectItem[];

  localidades: SelectItem[];

  provincias: SelectItem[];

  

  constructor(private fb: FormBuilder, 
    private messageService: MessageService) { }

  ngOnInit() {

    this.userform = this.fb.group({
      'name': new FormControl('', Validators.required),
      'doc': new FormControl('', Validators.required),
      'tel': new FormControl('', Validators.required),
      'vto': new FormControl('', Validators.required),
      'dom': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.required),
      'clase': new FormControl('', Validators.required),
      'localidad': new FormControl('', Validators.required),
      'provincia': new FormControl('', Validators.required)
    });
  
    this.clases = [];
    this.clases.push({label:'clase', value:'Clase'});
    this.clases.push({label:'A', value:'A'});
    this.clases.push({label:'B', value:'B'});
    this.clases.push({label:'B1', value:'B2'});

    this.localidades = [];
    this.localidades.push({label:'Selecciona Localidad', value:'Seleccion'});
    this.localidades.push({label:'San Martin', value:'SM'});
    this.localidades.push({label:'Vicente Lopez', value:'VL'});
    this.localidades.push({label:'San Fernando', value:'SF'});
    this.localidades.push({label:'Capital Federal', value:'CF'});

    this.provincias = [];
    this.provincias.push({label:'Selecciona Provincia', value:'Seleccion'});
    this.provincias.push({label:'Buenos Aires', value:'BA'});
    this.provincias.push({label:'San Luis', value:'SL'});
    this.provincias.push({label:'Santa Fe', value:'SF'});
    this.provincias.push({label:'La Rioja', value:'LR'});
    this.provincias.push({label:'Buenos Aires', value:'BA'});
    this.provincias.push({label:'San Luis', value:'SL'});
    this.provincias.push({label:'Santa Fe', value:'SF'});
    this.provincias.push({label:'La Rioja', value:'LR'});
    this.provincias.push({label:'Buenos Aires', value:'BA'});
    this.provincias.push({label:'San Luis', value:'SL'});
    this.provincias.push({label:'Santa Fe', value:'SF'});
    this.provincias.push({label:'La Rioja', value:'LR'});
    this.provincias.push({label:'Buenos Aires', value:'BA'});
    this.provincias.push({label:'San Luis', value:'SL'});
    this.provincias.push({label:'Santa Fe', value:'SF'});
    this.provincias.push({label:'La Rioja', value:'LR'});
    this.provincias.push({label:'Buenos Aires', value:'BA'});
    this.provincias.push({label:'San Luis', value:'SL'});
    this.provincias.push({label:'Santa Fe', value:'SF'});
    this.provincias.push({label:'La Rioja', value:'LR'});

  }

  onSubmit(value: string) {
    this.submitted = true;
    this.messageService.add({severity:'info', summary:'Success', detail:'Form Submitted'});
  }

  get diagnostic() { return JSON.stringify(this.userform.value); }

}