import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {

  uservehi: FormGroup; 

  userform: FormGroup;

  submitted: boolean;

  clases: SelectItem[];

  aseguradoras: SelectItem[];

  cond: SelectItem[];

  marcas: SelectItem[];

  modelos: SelectItem[];

  constructor(private fb: FormBuilder, 
    private messageService: MessageService) { }

  ngOnInit() {

    this.userform = this.fb.group({
      'name': new FormControl('', Validators.required),
      'nameConductor': new FormControl('', Validators.required),
      'doc': new FormControl('', Validators.required),
      'docConductor': new FormControl('', Validators.required),
      'tel': new FormControl('', Validators.required),
      'telConductor': new FormControl('', Validators.required),
      'mail': new FormControl('', Validators.required),
      'mailConductor': new FormControl('', Validators.required),
      'dom': new FormControl('', Validators.required),
      'cp': new FormControl('', Validators.required),
      'poliza': new FormControl('', Validators.required),
      'polizaConductor': new FormControl('', Validators.required),
      'patente': new FormControl('', Validators.required),
      'aseguradora': new FormControl('', Validators.required),
      'condicion': new FormControl('', Validators.required),
      'marca': new FormControl('', Validators.required),
      'modelo': new FormControl('', Validators.required)
    });

    this.aseguradoras = [];
    this.aseguradoras.push({label:'Selecciona Aseguradora', value:'Seleccion'});
    this.aseguradoras.push({label:'Mapfre', value:'SM'});
    this.aseguradoras.push({label:'Unigo', value:'VL'});
    this.aseguradoras.push({label:'La Nueva', value:'SF'});
    this.aseguradoras.push({label:'San Cristobal', value:'CF'});

    this.cond = [];
    this.cond.push({label:'Selecciona condiciòn del conductor', value:'Seleccion'});
    this.cond.push({label:'Titular', value:'TI'});
    this.cond.push({label:'Conyugue', value:'CO'});
    this.cond.push({label:'Hijo', value:'HI'});
    this.cond.push({label:'Otro', value:'OT'});

    this.marcas = [];
    this.marcas.push({label:'Selecciona Marca', value:'Seleccion'});
    this.marcas.push({label:'Peugeot', value:'P'});
    this.marcas.push({label:'Renault', value:'R'});
    this.marcas.push({label:'Citroen', value:'C'});
    this.marcas.push({label:'Jeep', value:'J'});

    this.modelos = [];
    this.modelos.push({label:'Selecciona Modelo', value:'Seleccion'});
    this.modelos.push({label:'Active 1.2', value:'P'});
    this.modelos.push({label:'Confort 1.5', value:'R'});
    this.modelos.push({label:'Active 1.6', value:'C'});
    this.modelos.push({label:'Allure', value:'J'});

  }

  onSubmit(value: string) {
    this.submitted = true;
    this.messageService.add({severity:'info', summary:'Success', detail:'Form Submitted'});
  }

  get diagnostic() { return JSON.stringify(this.userform.value); }

}
