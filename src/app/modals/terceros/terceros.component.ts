import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-terceros',
  templateUrl: './terceros.component.html',
  styleUrls: ['./terceros.component.css'],
  providers: [MessageService]
})
export class TercerosComponent implements OnInit {

  uservehi: FormGroup; 

  userform: FormGroup;

  submitted: boolean;

  afectados: SelectItem[];

  centros: SelectItem[];

  obras: SelectItem[];

  first_a= true;

  first_b= false;

  lesionado= false;

  constructor(private fb: FormBuilder, 
    private messageService: MessageService) { }

  ngOnInit() {


    this.userform = this.fb.group({
      'name': new FormControl('', Validators.required),
      'tel': new FormControl('', Validators.required),
      'afecta': new FormControl('', Validators.required),
      'obra': new FormControl('', Validators.required),
      'centro': new FormControl('', Validators.required)
    });
  
    this.afectados = [];
    this.afectados.push({label:'Tipo de relaciòn', value:'Seleccion'});
    this.afectados.push({label:'Conductor del vehículo contrario', value:'Conductor'});
    this.afectados.push({label:'Ocupante del vehículo contrario', value:'Ocupante'});
    this.afectados.push({label:'Peaton', value:'Peaton'});
    this.afectados.push({label:'Otros', value:'Otro'});

    this.centros = [];
    this.centros.push({label:'Selecciona Centro asistencial', value:'Seleccion'});
    this.centros.push({label:'Hospital Italiano', value:'HI'});
    this.centros.push({label:'Hospital Posadas', value:'HP'});
    this.centros.push({label:'Clinica del sol', value:'CDS'});
    this.centros.push({label:'Hospital Thompson', value:'HT'});

    this.obras = [];
    this.obras.push({label:'Selecciona Obra social / ART', value:'Seleccion'});
    this.obras.push({label:'Osde', value:'O'});
    this.obras.push({label:'Swiss Medical', value:'SM'});
    this.obras.push({label:'Omint', value:'OM'});
    this.obras.push({label:'Luis Pasteur', value:'LP'});

    this.centros = [];
    this.centros.push({label:'Selecciona Centro asistencial', value:'Seleccion'});
    this.centros.push({label:'Hospital Italiano', value:'HI'});
    this.centros.push({label:'Hospital Posadas', value:'HP'});
    this.centros.push({label:'Clinica del sol', value:'CDS'});
    this.centros.push({label:'Hospital Thompson', value:'HT'});

    this.obras = [];
    this.obras.push({label:'Selecciona Obra social / ART', value:'Seleccion'});
    this.obras.push({label:'Osde', value:'O'});
    this.obras.push({label:'Swiss Medical', value:'SM'});
    this.obras.push({label:'Omint', value:'OM'});
    this.obras.push({label:'Luis Pasteur', value:'LP'});
  
    }

    onSubmit(value: string) {
      this.submitted = true;
      this.messageService.add({severity:'info', summary:'Success', detail:'Form Submitted'});
    }
  
    get diagnostic() { return JSON.stringify(this.userform.value); }



  

}
