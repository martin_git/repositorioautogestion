import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogInitialComponent } from './log-initial.component';

describe('LogInitialComponent', () => {
  let component: LogInitialComponent;
  let fixture: ComponentFixture<LogInitialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogInitialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogInitialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
