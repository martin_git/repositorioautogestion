import { Component, OnInit } from '@angular/core';

interface Doc {
  name: string;
  code: string;
}


@Component({
  selector: 'app-log-initial',
  templateUrl: './log-initial.component.html',
  styleUrls: ['./log-initial.component.css']
})
export class LogInitialComponent implements OnInit {

  initial= false;
  recupero= false;
  registry= false;
  generate= false;
  query= true;
  question= false;
  questionTwo= false;
  questionThree= false;

document: Doc[];

selectedDoc: Doc;

selectedValue: string = 'val1';

  constructor() { 

    this.document = [
      {name: 'LC', code: 'LC'},
      {name: 'LE', code: 'LE'},
      {name: 'CI', code: 'CI'},
      {name: 'DNI', code: 'DNI'},
      {name: 'CUIT', code: 'CUIT'}
  ];


  }
  

    displayBasic: boolean;
    displayBasics: boolean;

    position: string;

    showBasicDialog() {
      this.displayBasic = true;
  }

    showBasicDialogs() {
      this.displayBasics = true;
  }
  
  ngOnInit() {
  }

  

}
